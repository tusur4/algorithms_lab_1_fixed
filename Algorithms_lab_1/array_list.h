#ifndef ARRAY_LIST_H
#define ARRAY_LIST_H

#include "shared_lib.h"

typedef struct array_type* Array_list;

Array_list array_list_create(unsigned int initial_size);
void array_list_add(Item data, Array_list *a);
Item array_list_get(int index, Array_list a);
int array_list_length(Array_list a);
int array_list_amount_elements(Array_list a);
void array_list_make_empty(Array_list *a);
void array_list_destroy(Array_list *a);

#endif
